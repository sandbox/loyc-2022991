-- SUMMARY --

This module allows site admin to add image/text popup banner.
Banner is shown to user once in specified time interval.


-- REQUIREMENTS --

Colorbox - https://drupal.org/project/colorbox
Cache pages for anonymous users must be OFF


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.
* Edit settings to activate popup banner - 
admin/config/user-interface/popup_banner


-- CONTACT --

Current maintainers:
* Kristaps Smelēns - https://drupal.org/user/402902
